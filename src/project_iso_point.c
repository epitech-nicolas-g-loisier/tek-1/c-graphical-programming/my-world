/*
** EPITECH PROJECT, 2018
** My World Bootstrap
** File description:
** Transforms a 3-dimensional point into a 2-dimensional one.
*/

#include <math.h>
#include <SFML/Graphics.h>
#include "my_world.h"

sfVector2f project_iso_point(int x, int y, int z, int new_scales[3])
{
	static int scales[3] = {SCALING_X, SCALING_Y, SCALING_Z};
	sfVector2f d_point;
	double angle_x = M_PI / 4;
	double angle_y = 25 * M_PI / 180;

	if (new_scales[0] != 0) {
		scales[0] = new_scales[0];
		scales[1] = new_scales[1];
		scales[2] = new_scales[2];
		return ((sfVector2f){0, 0});
	}
	x = x * scales[0];
	y = y * scales[1];
	z = z * scales[2];
	d_point.x = cos(angle_x) * x - cos(angle_x) * y + (WIDTH / 2);
	d_point.y = sin(angle_y) * y + sin(angle_y) * x - z;
	return (d_point);
}
