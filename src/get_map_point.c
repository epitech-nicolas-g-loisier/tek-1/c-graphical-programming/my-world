/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Returns the point wanted
*/

#include <stdlib.h>
#include "my_world.h"

sfVector2f	get_map_point(sfVector2f **map, int pos, int size_x, int offset)
{
	sfVector2f point = {0, 0};

	if (!map)
		return (point);
	point.x = map[pos][size_x].x;
	point.y = map[pos][size_x].y + offset;
	return (point);
}
