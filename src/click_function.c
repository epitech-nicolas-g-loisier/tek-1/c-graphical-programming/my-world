/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Click function
*/

#include "my_world.h"

void	modif_corner(int *heigth, int modif)
{
	if (*heigth < MAX_Z && modif == 1) {
		*heigth += modif;
		play_sound(0);
	} else if (*heigth > MIN_Z && modif == -1) {
		*heigth += modif;
		play_sound(1);
	}
}

int	**modif_tile(int **map, sfVector2i pos, int modif)
{
	if (modif == 0){
		map[pos.y][pos.x] = 0;
		map[pos.y][(pos.x + 1)] = 0;
		map[(pos.y + 1)][pos.x] = 0;
		map[(pos.y + 1)][(pos.x + 1)] = 0;
		play_sound(2);
	} else {
		modif_corner(&map[pos.y][pos.x], modif);
		modif_corner(&map[pos.y][(pos.x + 1)], modif);
		modif_corner(&map[(pos.y + 1)][pos.x], modif);
		modif_corner(&map[(pos.y + 1)][(pos.x + 1)], modif);
	}
	return (map);
}

int	**terra_tile(sfVector2i mouse, int **map_3d, sfVector2f **map_2d,
			sfVector2i *size)
{
	sfVector2i pos = find_tile_clicked(mouse, map_2d, size);
	sfVector2i choice = selected_button(0, 0);

	if (pos.x != -1 && choice.x == 0)
		map_3d = modif_tile(map_3d, pos, 1);
	else if (pos.x != -1 && choice.x == 1)
		map_3d = modif_tile(map_3d, pos, -1);
	else
		if (choice.x == 2 && pos.x != -1)
			map_3d = modif_tile(map_3d, pos, 0);
	return (map_3d);
}

int	**terra_corner(sfVector2i mouse, int **map_3d, sfVector2f **map_2d,
			sfVector2i *size)
{
	sfVector2i pos = find_corner_clicked(mouse, map_2d, size);
	sfVector2i choice = selected_button(0, 0);

	if (pos.x != -1 && choice.x == 0 && map_3d[pos.y][pos.x] < MAX_Z)
		modif_corner(&map_3d[pos.y][pos.x], 1);
	else if (pos.x != -1 && choice.x == 1 && map_3d[pos.y][pos.x] > MIN_Z)
		modif_corner(&map_3d[pos.y][pos.x], -1);
	else
		if (choice.x == 2 && pos.x != -1) {
			map_3d[pos.y][pos.x] = 0;
			play_sound(2);
		}
	return (map_3d);
}

int	**check_map_hit(sfRenderWindow *window, int **map_3d,
				sfVector2f **map_2d, sfVector2i *size)
{
	sfVector2i mouse = sfMouse_getPosition((sfWindow*)window);
	int **(*function[2])(sfVector2i, int**, sfVector2f**, sfVector2i*);
	sfVector2i choice = selected_button(0, 0);
	static sfClock *clock = NULL;
	static sfTime time;

	if (clock == NULL) {
		clock = sfClock_create();
		time.microseconds = 250000;
	}
	function[0] = &terra_corner;
	function[1] = &terra_tile;
	if (time.microseconds > 250000) {
		map_3d = function[choice.y](mouse, map_3d, map_2d, size);
		sfClock_restart(clock);
	}
	time = sfClock_getElapsedTime(clock);
	return (map_3d);
}
