/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Close Button
*/

#include "my_world.h"

void	ask_save(sfRenderWindow *window, int **map3d, sfVector2f **map_2d,
			sfVector2i *size)
{
	int stop = 0;

	stop = display_save_popup(window, map3d, map_2d, size);
	if (stop == 0) {
		save_map(map3d, NULL, size);
		sfRenderWindow_close(window);
	}
	else if (stop == 1) {
		sfRenderWindow_close(window);
	}
}

int	close_button(sfRenderWindow *window, int **map3d, sfVector2f **map_2d,
			sfVector2i *size)
{
	sfVector2f pos = {1775, 25};
	sfVector2i mouse = sfMouse_getPosition((sfWindow*)window);

	if (sfMouse_isButtonPressed(sfMouseLeft) == sfTrue
		&& check_mouse_pos(mouse, pos) == 1) {
		ask_save(window, map3d, map_2d, size);
		return (1);
	}
	else if (sfKeyboard_isKeyPressed(sfKeyEscape) == sfTrue) {
		ask_save(window, map3d, map_2d, size);
		return (1);
	}
	return (0);
}
