/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Checks if the clock is created
*/

#include "my_world.h"

sfClock	*check_init_clock(sfClock *clock, int *time)
{
	if (time == NULL)
		return (NULL);
	if (clock == NULL) {
		clock = sfClock_create();
		*time = 3;
	} else {
		*time = sfTime_asSeconds(sfClock_getElapsedTime(clock));
	}
	return (clock);
}
