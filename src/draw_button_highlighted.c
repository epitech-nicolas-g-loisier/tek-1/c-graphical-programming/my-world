/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Draw hightlight and help
*/

#include "my_world.h"

void	draw_button_help(sfRenderWindow *window, int i, sfVector2i mouse)
{
	char str[9][40] = {"asset/infobulle_raise_ground.png\0",
				"asset/infobulle_decrease_ground.png\0",
				"asset/infobulle_set_zero_heigth.png\0",
				"asset/infobulle_deselect_terra_tool.png\0",
				"asset/infobulle_select_corner.png\0",
				"asset/infobulle_select_tile.png\0",
				"asset/infobulle_exit.png\0",
				"asset/infobulle_save.png\0",
				"asset/infobulle_observer_mode.png\0"};
	sfVector2f pos = {mouse.x, mouse.y};

	if (i < 6) {
		pos.y += 25;
		draw_button_sprite(window, str[i], pos);
	}
	else {
		pos.y += 25;
		pos.x -= 200;
		draw_button_sprite(window, str[i], pos);
	}
}

int	highlighted_button(sfRenderWindow *window, sfVector2i mouse)
{
	sfVector2f pos[9] = {{25, 25}, {25, 150}, {25, 275}, {25, 400},
				{25, 525}, {25, 650}, {1775, 25}, {1775, 150},
				{1775, 955}};
	char *str = "asset/button_state_highlight.png\0";
	int i = 0;

	while (i < 9) {
		if (check_mouse_pos(mouse, pos[i]) == 1) {
			draw_button_sprite(window, str, pos[i]);
			draw_button_help(window, i, mouse);
			return (1);
		}
		i++;
	}
	return (0);
}
