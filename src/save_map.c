/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Saves the map in a file
*/

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "my_world.h"
#include "my.h"

static int	putnbr_fd(const int nbr, const int fd)
{
	char conv_nbr[12] = "";
	int rank = 1000000000;
	int count = 0;

	if (nbr < 0) {
		conv_nbr[count] = '-';
		count++;
	}
	while (rank > 1 && nbr / rank == 0)
		rank = rank / 10;
	while (rank > 0) {
		if (nbr < 0)
			conv_nbr[count] = '0' + nbr * -1 / rank % 10;
		else
			conv_nbr[count] = '0' + nbr / rank % 10;
		count++;
		rank = rank / 10;
	}
	conv_nbr[count] = '\0';
	return (write(fd, conv_nbr, my_strlen(conv_nbr)));
}

int	write_map_on_file(int * const *map, const sfVector2i *size, int fd)
{
	sfVector2i count = {0, 0};
	int write_status[2] = {0};

	for (count.y = 0; count.y <= size->y; count.y = count.y + 1) {
		for (count.x = 0; count.x <= size->x; count.x = count.x + 1) {
			write_status[0] = putnbr_fd(map[count.y][count.x], fd);
			if (count.x < size->x && write_status[0] != -1)
				write_status[1] = write(fd, " ", 1);
			else if (write_status[0] != -1 && count.y < size->y)
				write_status[1] = write(fd, "\n", 1);
			if (write_status[0] == -1 || write_status[1] == -1) {
				close(fd);
				return (84);
			}
		}
	}
	close(fd);
	return (0);
}

int	save_map(int * const *map, const char *path, sfVector2i *size)
{
	int fd = -1;
	static char *keep_path = NULL;

	if (keep_path == NULL) {
		keep_path = (char*)path;
		return (0);
	}
	fd = open(keep_path, O_RDWR | O_TRUNC | O_CREAT);
	if (fd == -1)
		return (84);
	return (write_map_on_file(map, size, fd));
}
