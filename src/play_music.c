/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Play music
*/

#include "my_world.h"

void	play_sound(int choice)
{
	char sound[3][21] = {"asset/up_sound.ogg\0",
				"asset/down_sound.ogg\0",
				"asset/zero_sound.ogg\0"};
	static sfMusic *music;

	if (music != NULL) {
		sfMusic_destroy(music);
	}
	if (choice != -1) {
		music = sfMusic_createFromFile(sound[choice]);
		sfMusic_setVolume(music, 10);
		sfMusic_play(music);
	}

}

void	play_music(int choice)
{
	char str[30] = "asset/rainforest_ambience.ogg\0";
	static sfMusic *music;

	if (choice == 0) {
		music = sfMusic_createFromFile(str);
		sfMusic_setLoop(music, sfTrue);
		sfMusic_setVolume(music, 2.5);
		sfMusic_play(music);
	}
	else {
		sfMusic_destroy(music);
	}
}
