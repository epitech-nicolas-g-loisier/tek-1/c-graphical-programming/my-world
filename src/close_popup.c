/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Close Button
*/

#include "my_world.h"

int	check_popup_pos(sfVector2i mouse_pos, sfVector2f button_pos)
{
	if (mouse_pos.x > (button_pos.x + 100) ||
		mouse_pos.y > (button_pos.y + 50)) {
		return (0);
	}
	if (mouse_pos.x < button_pos.x || mouse_pos.y < button_pos.y) {
		return (0);
	}
	return (1);
}

void	check_popup_highlight(sfRenderWindow *window)
{
	sfVector2f pos[3] = {{785, 565}, {910, 565}, {1035, 565}};
	sfVector2i mouse = sfMouse_getPosition((sfWindow*)window);
	int count = 0;
	char str[26] = "asset/popup_highlight.png\0";

	while (count < 3) {
		if (check_popup_pos(mouse, pos[count]) == 1) {
			draw_button_sprite(window, str, pos[count]);
			break;
		}
		count++;
	}
}

int	check_popup_click(sfRenderWindow *window)
{
	sfVector2f pos[3] = {{785, 565}, {910, 565}, {1035, 565}};
	sfVector2i mouse = sfMouse_getPosition((sfWindow*)window);
	int count = 0;
	char str[24] = "asset/popup_pressed.png\0";

	while (count < 3) {
		if (sfMouse_isButtonPressed(sfMouseLeft) == sfTrue
			&& check_popup_pos(mouse, pos[count]) == 1) {
			draw_button_sprite(window, str, pos[count]);
			return (count);
		}
		count++;
	}
	return (-1);
}

int	display_save_popup(sfRenderWindow *window, int **map3d,
				sfVector2f **map_2d, sfVector2i *size)
{
	char str[21] = "asset/save_popup.png\0";
	sfTexture *texture = sfTexture_createFromFile(str, NULL);
	sfSprite *sprite = sfSprite_create();
	int stop = -1;

	sfSprite_setTexture(sprite, texture, sfTrue);
	sfSprite_setPosition(sprite, (sfVector2f){760, 440});
	sfSleep((sfTime){250000});
	sfMouse_setPosition((sfVector2i){835, 590}, (sfWindow*)window);
	while (stop == -1) {
		draw_element(window, map3d, map_2d, size);
		stop = check_popup_click(window);
		sfRenderWindow_drawSprite(window, sprite, NULL);
		check_popup_highlight(window);
		sfRenderWindow_display(window);
	}
	sfTexture_destroy(texture);
	sfSprite_destroy(sprite);
	return (stop);
}
