/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Searches the map to find if any tiles are clicked
*/

#include <stdbool.h>
#include "my_world.h"

static bool	corner_is_inbound(sfVector2f *pos)
{
	if (pos->x < -75 || pos->x > WIDTH + 75)
		return (false);
	if (pos->y < -75 || pos->y > HEIGHT + 75)
		return (false);
	return (true);
}

bool	tile_is_inbound(sfVector2f **map, sfVector2i *tile_pos)
{
	int count = 0;
	sfVector2i progress;

	while (count < 4) {
		progress.x = tile_pos->x + count % 2;
		progress.y = tile_pos->y + count / 2;
		if (corner_is_inbound(&map[progress.y][progress.x]))
			return (true);
		count++;
	}
	return (false);
}

static bool	are_signs_equal(int signs[4])
{
	int i = 0;

	for (i = 0; i < 4; i = i + 1) {
		if (signs[i] != signs[(i + 1) % 4])
			return (false);
	}
	return (true);
}

static bool	tile_is_clicked(sfVector2f **map, sfVector2i *tile_pos,
			const sfVector2i *click_pos)
{
	sfVector2f points[4];
	int signs[4] = {0, 0, 0, 0};
	int i = 0;

	if (tile_is_inbound(map, tile_pos) == false)
		return (false);
	points[0] = map[tile_pos->y + 1][tile_pos->x];
	points[1] = map[tile_pos->y + 1][tile_pos->x + 1];
	points[2] = map[tile_pos->y][tile_pos->x + 1];
	points[3] = map[tile_pos->y][tile_pos->x];
	for (i = 0; i < 4; i = i + 1) {
		points[i].x -= click_pos->x;
		points[i].y -= click_pos->y;
	}
	for (i = 0; i < 4; i = i + 1)
		signs[i] = points[(i + 1) % 4].x * points[i].y
			- points[i].x * points[(i + 1) % 4].y > 0;
	return (are_signs_equal(signs));
}

sfVector2i	find_tile_clicked(sfVector2i click_pos, sfVector2f **map,
			const sfVector2i *size)
{
	sfVector2i tile_pos = *size;

	while (--tile_pos.y >= 0) {
		tile_pos.x = size->x;
		while (--tile_pos.x >= 0) {
			if (tile_is_clicked(map, &tile_pos, &click_pos))
				return (tile_pos);
		}
	}
	return (tile_pos);
}
