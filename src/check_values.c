/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Checks and corrects values in the 3d map
*/

#include <stdlib.h>
#include "my_world.h"

int	**check_values(int **map, sfVector2i *size)
{
	int x = -1;
	int y = -1;

	if (!map || !size)
		return (NULL);
	while (++y <= size->y) {
		x = -1;
		while (++x <= size->x) {
			if (map[y][x] < MIN_Z)
				map[y][x] = MIN_Z;
			if (map[y][x] > MAX_Z)
				map[y][x] = MAX_Z;
		}
	}
	return (map);
}
