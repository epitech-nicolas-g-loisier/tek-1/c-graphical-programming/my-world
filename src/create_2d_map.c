/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Create a 2d map using a 3d map
*/

#include <stdlib.h>
#include "my_world.h"

void	free_3d_map(int **map, sfVector2i *size)
{
	int y = -1;

	while (++y <= size->y)
		free(map[y]);
	free(map);
}

void	free_2d_map(sfVector2f **map, sfVector2i *size)
{
	int y = -1;

	while (++y <= size->y)
		free(map[y]);
	free(map);
}

sfVector2f	**center_map(sfVector2f **map_2d, sfVector2i *size)
{
	int x = 0;
	int y = 0;
	int move = (1080 - (map_2d[0][0].y + map_2d[size->y][size->x].y)) / 2;

	while (y <= size->y) {
		x = 0;
		while (x <= size->x) {
			map_2d[y][x].y += move;
			x++;
		}
		y++;
	}
	return (map_2d);
}

sfVector2f	**malloc_2d_map(sfVector2f **map, sfVector2i *size)
{
	int count = -1;

	while (++count <= size->y) {
		map[count] = malloc(sizeof(sfVector2f) * (size->x + 1));
		if (map[count] == NULL) {
			size->y = count;
			free_2d_map(map, size);
			return (NULL);
		}
	}
	return (map);
}

sfVector2f	**create_2d_map(int **map_3d, sfVector2i *size)
{
	sfVector2f **map2d = malloc(sizeof(sfVector2f *) * (size->y + 2));

	if (map2d == NULL)
		return (NULL);
	map2d = malloc_2d_map(map2d, size);
	if (map2d == NULL)
		return (NULL);
	map2d = update_2d_map(map2d, size, map_3d);
	return (map2d);
}
