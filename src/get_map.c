/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Opens the savefile and loads the map
*/

#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include "my_world.h"
#include "my.h"

int	get_nbr(char *line, int idx)
{
	int nbr_c = 0;
	int count = line[0] == '-';
	int return_v = 0;

	if (line[count] >= '0' && line[count] <= '9')
		nbr_c = 1;
	while (nbr_c <= idx) {
		if (line[count] == ' ' || line[count] == '\t') {
			if (line[count + 1] == '-')
				count++;
			if (line[count + 1] >= '0' && line[count + 1] <= '9')
				nbr_c++;
		}
		count++;
	}
	if (count > 0 && line[count - 1] == '-')
		count--;
	return_v = my_getnbr(&line[count]);
	return (return_v);
}

static int	**print_err_mem_full(char *line, int **map, sfVector2i *idx)
{
	free(line);
	if (idx != NULL) {
		idx->y--;
		while (idx->y >= 0) {
			free(map[idx->y]);
			idx->y--;
		}
		free(map);
	}
	write(2, "error: memory full\n", 19);
	return (NULL);
}

int	**get_map(char *path, sfVector2i size)
{
	char *line = NULL;
	sfVector2i idx = {-1, -1};
	int **map = malloc(sizeof(int *) * size.y);
	int fd = open(path, O_RDWR);

	if (map == NULL || fd == -1)
		return (print_err_mem_full(NULL, NULL, NULL));
	while (++idx.y < size.y) {
		idx.x = -1;
		map[idx.y] = malloc(sizeof(int) * size.x);
		line = get_next_line(fd);
		if (line == NULL || map[idx.y] == NULL)
			return (print_err_mem_full(line, map, &idx));
		while (++idx.x < size.x)
			map[idx.y][idx.x] = get_nbr(line, idx.x);
		free(line);
		line = NULL;
	}
	close(fd);
	return (map);
}
