/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Create and draw map border
*/

#include <unistd.h>
#include <stdlib.h>
#include "my_world.h"

sfConvexShape *draw_right_square(int **map_3d, sfVector2f **map_2d, int pos[2],
				sfVector2i size)
{
	sfConvexShape* border;
	sfVector2f right;
	sfVector2f down;
	static int scale = SCALING_Z;
	int right_coord = (map_3d[pos[0]][size.x] * scale) + (16 * scale);
	int down_coord = (map_3d[(pos[0] + 1)][size.x] * scale) + 16 * scale;

	if (pos[1] != 0) {
		scale = pos[1];
		return (NULL);
	}
	border = sfConvexShape_create();
	right = get_map_point(map_2d, pos[0], size.x, right_coord);
	down = get_map_point(map_2d, pos[0] + 1, size.x, down_coord);
	sfConvexShape_setPointCount(border, 4);
	sfConvexShape_setPoint(border, 0, map_2d[pos[0]][size.x]);
	sfConvexShape_setPoint(border, 1, map_2d[(pos[0] + 1)][size.x]);
	sfConvexShape_setPoint(border, 2, down);
	sfConvexShape_setPoint(border, 3, right);
	return (border);
}

void	create_right_border(sfVector2f **map_2d, sfVector2i size, int **map_3d,
			sfRenderWindow *window)
{
	sfConvexShape* border;
	int idx[2] = {0, 0};
	int max = size.x;
	sfColor color = {159, 99, 49, 255};
	sfVertexArray *line;

	while (idx[0] < size.y) {
		border = draw_right_square(map_3d, map_2d, idx, size);
		sfConvexShape_setFillColor(border, color);
		sfRenderWindow_drawConvexShape(window, border, NULL);
		sfConvexShape_destroy(border);
		line = create_line(&map_2d[idx[0]][max],
				&map_2d[(idx[0] + 1)][max]);
		sfRenderWindow_drawVertexArray(window, line, NULL);
		sfVertexArray_destroy(line);
		idx[0]++;
	}
}

sfConvexShape *draw_left_square(int *map_3d, sfVector2f *map_2d, int pos,
				int new_scale)
{
	sfConvexShape* border;
	sfVector2f left;
	sfVector2f down;
	static int scale = SCALING_Z;
	int left_coord = (map_3d[pos] * scale) + (16 * scale);
	int down_coord = (map_3d[(pos + 1)] * scale) + 16 * scale;

	if (new_scale != 0) {
		scale = new_scale;
		return (NULL);
	}
	border = sfConvexShape_create();
	left = (sfVector2f){map_2d[pos].x, map_2d[pos].y + left_coord};
	down = (sfVector2f){map_2d[(pos + 1)].x, map_2d[(pos+1)].y+down_coord};
	sfConvexShape_setPointCount(border, 4);
	sfConvexShape_setPoint(border, 0, map_2d[pos]);
	sfConvexShape_setPoint(border, 1, map_2d[(pos + 1)]);
	sfConvexShape_setPoint(border, 2, down);
	sfConvexShape_setPoint(border, 3, left);
	return (border);
}

void	create_left_border(sfVector2f *map_2d, sfVector2i size, int *map_3d,
				sfRenderWindow *window)
{
	sfConvexShape* border;
	int idx = 0;
	sfColor color = {139, 79, 29, 255};
	sfVertexArray *line;

	while (idx < size.x) {
		border = draw_left_square(map_3d, map_2d, idx, 0);
		sfConvexShape_setFillColor(border, color);
		sfRenderWindow_drawConvexShape(window, border, NULL);
		sfConvexShape_destroy(border);
		line = create_line(&map_2d[idx], &map_2d[(idx + 1)]);
		sfRenderWindow_drawVertexArray(window, line, NULL);
		sfVertexArray_destroy(line);
		idx++;
	}
}

void	draw_map_border(sfRenderWindow *window, sfVector2f **map_2d,
			sfVector2i size, int **map_3d)
{
	create_left_border(map_2d[size.y], size, map_3d[size.y], window);
	create_right_border(map_2d, size, map_3d, window);
}
