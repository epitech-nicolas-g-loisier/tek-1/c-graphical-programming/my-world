/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Shortcuts for button
*/

#include "my_world.h"

void	get_shortcuts(void)
{
	sfKeyCode key[6] = {sfKeyA, sfKeyZ, sfKeyE, sfKeyR, sfKeyNum1,
				sfKeyNum2};
	int idx = 0;

	while (idx < 6) {
		if (sfKeyboard_isKeyPressed(key[idx]) == sfTrue) {
			selected_button(idx, 1);
			break;
		}
		idx++;
	}
}
