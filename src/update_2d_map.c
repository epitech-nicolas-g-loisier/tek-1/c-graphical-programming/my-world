/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Updates the 2d map
*/

#include "my_world.h"

sfVector2f	**update_2d_map(sfVector2f **map2d, sfVector2i *size,
				int **map3d)
{
	int x = -1;
	int y = -1;
	int scales[3] = {0, 0, 0};

	while (++y <= size->y) {
		x = -1;
		while (++x <= size->x)
			map2d[y][x] = project_iso_point(x , y, map3d[y][x],
							scales);
	}
	map2d[y] = NULL;
	map2d = center_map(map2d, size);
	return (map2d);
}
