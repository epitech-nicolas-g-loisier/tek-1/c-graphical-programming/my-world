/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Find corner
*/

#include "my_world.h"

int	check_click(sfVector2i pos_2d, sfVector2i map_pos)
{
	if (pos_2d.x < (map_pos.x - 32) || pos_2d.y < (map_pos.y - 32)) {
		return (0);
	}
	if (pos_2d.x > (map_pos.x + 32) || pos_2d.y > (map_pos.y + 32)) {
		return (0);
	}
	return (1);
}

sfVector2i	find_corner_clicked(sfVector2i pos_2d, sfVector2f **map_2d,
					sfVector2i *size)
{
	sfVector2i idx = {size->x, size->y};
	sfVector2i map_pos;
	sfVector2i pos = {-1, -1};

	while (idx.y >= 0) {
		idx.x = size->x;
		while (idx.x >= 0) {
			map_pos.x = map_2d[idx.y][idx.x].x;
			map_pos.y = map_2d[idx.y][idx.x].y;
			if (check_click(pos_2d, map_pos) == 1) {
				pos.x = idx.x;
				pos.y = idx.y;
				return (pos);
			}
			idx.x--;
		}
		idx.y--;
	}
	return (pos);
}
