/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Check window event
*/

#include <stdbool.h>
#include "my_world.h"

static int	update_move_size(int move_size, int new_move_size)
{
	if (new_move_size != 0)
		move_size = new_move_size;
	return (move_size);
}

void	get_keyboard_event(sfVector2i *move, int new_move_size,
				sfVector2f **map_2d, sfVector2i *size)
{
	static int move_size = 5;

	move_size = update_move_size(move_size, new_move_size);
	if (move != NULL && sfKeyboard_isKeyPressed(sfKeyUp) == sfTrue
		&& (map_2d[0][0].y) < (HEIGHT / 2)) {
		move->y += move_size;
	} else if (move != NULL && sfKeyboard_isKeyPressed(sfKeyDown) == sfTrue
			&& (map_2d[size->y][size->x].y) > (HEIGHT /2)) {
		move->y -= move_size;
	}
	if (move != NULL && sfKeyboard_isKeyPressed(sfKeyLeft) == sfTrue
		&& (map_2d[size->y][0].x) < (WIDTH / 2)) {
		move->x += move_size;
	} else if (move != NULL && sfKeyboard_isKeyPressed(sfKeyRight) == sfTrue
			&& (map_2d[0][size->x].x) > (WIDTH / 2)) {
		move->x -= move_size;
	}
	get_shortcuts();
}

int	draw_obs_button(sfRenderWindow *window, bool *is_button_clicked,
			sfClock *cooldown)
{
	sfVector2f pos = {1775, 955};

	draw_button_sprite(window, "asset/button_observer.png\0", pos);
	if (sfTime_asSeconds(sfClock_getElapsedTime(cooldown)) <= 1) {
		draw_button_sprite(window, "asset/button_state_click.png", pos);
		return (0);
	}
	if (is_obs_button_clicked(window)) {
		draw_button_sprite(window, "asset/button_state_click.png", pos);
		*is_button_clicked = true;
		return (1);
	}
	return (0);
}

int	observer_button(sfRenderWindow *window, int **map_3d,
			sfVector2i **size, sfVector2f **map_2d)
{
	bool obs_button_clicked = 0;
	sfVector2i *move = size[1];
	sfClock *cooldown = sfClock_create();

	set_viewpoint(map_3d, size[0]);
	while (!obs_button_clicked) {
		get_zoom_input(map_3d, size[0], 0);
		map_2d = update_2d_map(map_2d, size[0], map_3d);
		map_2d = move_map(map_2d, size[0], move);
		sfRenderWindow_clear(window, (sfColor){125, 125, 175, 255});
		draw_texture(window, map_2d, size[0]);
		draw_map_border(window, map_2d, *size[0], map_3d);
		draw_obs_button(window, &obs_button_clicked, cooldown);
		get_keyboard_event(move, 0, map_2d, size[0]);
		sfRenderWindow_display(window);
	}
	reset_viewpoint(map_3d, size[0]);
	sfClock_destroy(cooldown);
	return (1);
}

void	get_mouse_event(sfRenderWindow *window, int **map_3d,
			sfVector2f **map_2d, sfVector2i **size)
{
	int button = 0;
	int time = 0;
	static sfClock *cooldown = NULL;

	button += check_button_state(window);
	button += save_button(window, map_3d, size[0]);
	button += close_button(window, map_3d, map_2d, size[0]);
	if (is_obs_button_clicked(window)) {
		cooldown = check_init_clock(cooldown, &time);
		if (time > 1) {
			button += observer_button(window, map_3d, size,
						&map_2d[0]);
			sfClock_restart(cooldown);
		}
	}
	if (button == 0 && sfMouse_isButtonPressed(sfMouseLeft) == sfTrue)
		check_map_hit(window, map_3d, map_2d, size[0]);
}
