/*
** EPITECH PROJECT, 2018
** My World Bootstrap
** File description:
** draw 2d map in iso
*/

#include <SFML/Graphics.h>
#include "my_world.h"

sfVertexArray	*create_line(sfVector2f *point1, sfVector2f *point2)
{
	sfVertexArray *vertex_array = sfVertexArray_create();
	sfVertex vertex1;
	sfVertex vertex2;

	vertex1.position = *point1;
	vertex1.color = sfWhite;
	vertex2.position = *point2;
	vertex2.color = sfWhite;
	sfVertexArray_append(vertex_array, vertex1);
	sfVertexArray_append(vertex_array, vertex2);
	sfVertexArray_setPrimitiveType(vertex_array, sfLinesStrip);
	return (vertex_array);
}

void	draw_line(sfVector2f **map, sfVector2i idx, sfVector2i size,
		sfRenderWindow *window)
{
	sfVertexArray *vertex;
	int x = idx.x;
	int y = idx.y;

	if (x < size.x) {
		vertex = create_line(&map[y][x], &map[y][x + 1]);
		sfRenderWindow_drawVertexArray(window, vertex, NULL);
		sfVertexArray_destroy(vertex);
	}
	if (y < size.y) {
		vertex = create_line(&map[y][x], &map[y + 1][x]);
		sfRenderWindow_drawVertexArray(window, vertex, NULL);
		sfVertexArray_destroy(vertex);
	}
}

int	draw_2d_map(sfRenderWindow *window, sfVector2f **map_2d,
		sfVector2i size)
{
	sfVector2i idx = {0, 0};

	while (idx.y <= size.y) {
		idx.x = -1;
		while (++idx.x <= size.x)
			draw_line(&map_2d[0], idx, size, window);
		idx.y++;
	}
	return (0);
}
