/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Create texture for each square
*/

#include <unistd.h>
#include <stdlib.h>
#include "my_world.h"

void	draw_outline(sfRenderWindow *window, sfVector2f *line1,
			sfVector2f *line2)
{
	sfVertexArray *vertex;

	vertex = create_line(&line1[0], &line1[1]);
	sfRenderWindow_drawVertexArray(window, vertex, NULL);
	sfVertexArray_destroy(vertex);
	vertex = create_line(&line1[0], &line2[0]);
	sfRenderWindow_drawVertexArray(window, vertex, NULL);
	sfVertexArray_destroy(vertex);
	vertex = create_line(&line1[1], &line2[1]);
	sfRenderWindow_drawVertexArray(window, vertex, NULL);
	sfVertexArray_destroy(vertex);
	vertex = create_line(&line2[0], &line2[1]);
	sfRenderWindow_drawVertexArray(window, vertex, NULL);
	sfVertexArray_destroy(vertex);
}

void	draw_square(sfRenderWindow *window, sfVector2f *line1,
		sfVector2f *line2, sfTexture *texture)
{
	sfConvexShape* square = sfConvexShape_create();

	sfConvexShape_setPointCount(square, 3);
	sfConvexShape_setPoint(square, 0, line1[0]);
	sfConvexShape_setPoint(square, 1, line1[1]);
	sfConvexShape_setPoint(square, 2, line2[1]);
	sfConvexShape_setTexture(square, texture, sfTrue);
	sfRenderWindow_drawConvexShape(window, square, NULL);
	sfConvexShape_setPoint(square, 1, line2[1]);
	sfConvexShape_setPoint(square, 2, line2[0]);
	sfRenderWindow_drawConvexShape(window, square, NULL);
	sfConvexShape_destroy(square);
	draw_outline(window, line1, line2);
}

void	draw_texture(sfRenderWindow *window, sfVector2f **map_2d,
			sfVector2i *size)
{
	sfVector2i idx = {0, 0};
	char *texture_path = "asset/texture_grass.png";
	sfTexture *texture = sfTexture_createFromFile(texture_path, NULL);

	for (idx.y = 0; idx.y < size->y; idx.y = idx.y + 1) {
		for (idx.x = 0; idx.x < size->x; idx.x = idx.x + 1) {
			if (tile_is_inbound(map_2d, &idx)) {
				draw_square(window, &map_2d[idx.y][idx.x],
					&map_2d[(idx.y + 1)][idx.x],
					texture);
			}
		}
	}
	sfTexture_destroy(texture);
}
