/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Checks if the observator button is pressed
*/

#include <stdbool.h>
#include "my_world.h"

bool	is_obs_button_clicked(sfRenderWindow *window)
{
	sfVector2i mouse = sfMouse_getPosition((sfWindow *)window);
	sfVector2f pos = {1775, 955};
	sfBool left_button_is_clicked = sfMouse_isButtonPressed(sfMouseLeft);

	if (left_button_is_clicked == sfTrue && check_mouse_pos(mouse, pos))
		return (true);
	if (sfKeyboard_isKeyPressed(sfKeyO) == sfTrue)
		return (true);
	return (false);
}
