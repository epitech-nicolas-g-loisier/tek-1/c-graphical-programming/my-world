/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Checks if the user asks to zoom
*/

#include "my_world.h"

static int	zoom_in(double scales[3], int **map_3d, sfVector2i *size)
{
	if (sfKeyboard_isKeyPressed(sfKeyPageUp) != sfTrue || scales[2] >= 32)
		return (0);
	scales[0] = scales[0] * 1.2;
	scales[1] = scales[1] * 1.2;
	scales[2] = scales[2] * 1.2;
	project_iso_point(0, 0, 0, (int [3]){scales[0], scales[1], scales[2]});
	draw_right_square(map_3d, NULL, (int [2]){0, scales[2]}, *size);
	draw_left_square(map_3d[0], NULL, 0, scales[2]);
	return (0);
}

static int	zoom_out(double scales[3], int **map_3d, sfVector2i *size)
{
	if (sfKeyboard_isKeyPressed(sfKeyPageDown) != sfTrue || scales[2] <= 2)
		return (0);
	scales[0] = scales[0] / 1.2;
	scales[1] = scales[1] / 1.2;
	scales[2] = scales[2] / 1.2;
	project_iso_point(0, 0, 0, (int [3]){scales[0], scales[1], scales[2]});
	draw_right_square(map_3d, NULL, (int [2]){0, scales[2]}, *size);
	draw_left_square(map_3d[0], NULL, 0, scales[2]);
	return (0);
}

int	get_zoom_input(int **map_3d, sfVector2i *size, int load_scales)
{
	static double scales[3] = {SCALING_X, SCALING_Y, SCALING_Z};
	double scales_copy[3] = {scales[0], scales[1], scales[2]};

	if (load_scales == 0) {
		zoom_in(&scales_copy[0], map_3d, size);
		zoom_out(&scales_copy[0], map_3d, size);
		scales[0] = scales_copy[0];
		scales[1] = scales_copy[1];
		scales[2] = scales_copy[2];
		return (0);
	}
	project_iso_point(0, 0, 0, (int [3]){scales[0], scales[1], scales[2]});
	draw_right_square(map_3d, NULL, (int [2]){0, scales[2]}, *size);
	draw_left_square(map_3d[0], NULL, 0, scales[2]);
	return (0);
}
