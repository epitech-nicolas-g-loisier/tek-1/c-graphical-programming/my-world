/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Selected button
*/

#include "my_world.h"

sfVector2i	selected_button(int i, int mode)
{
	static sfVector2i button = {3, 0};

	if (mode == 1) {
		if (i < 4)
			button.x = i;
		else if (i > 3 && i < 6)
			button.y = i - 4;
	}
	return (button);
}

void	draw_selected_button(sfRenderWindow *window)
{
	char *str = "asset/button_state_selected.png\0";
	sfVector2f terr_button[4] = {{25, 25}, {25, 150}, {25, 275}, {25, 400}};
	sfVector2f select_button[2] = {{25, 525}, {25, 650}};
	sfVector2i chosen_button = selected_button(0, 0);
	sfTexture *texture = sfTexture_createFromFile(str, NULL);;
	sfSprite *sprite = sfSprite_create();

	sfSprite_setTexture(sprite, texture, sfTrue);
	if (chosen_button.x != -1) {
		sfSprite_setPosition(sprite, terr_button[chosen_button.x]);
		sfRenderWindow_drawSprite(window, sprite, NULL);
	}
	if (chosen_button.y != -1) {
		sfSprite_setPosition(sprite, select_button[chosen_button.y]);
		sfRenderWindow_drawSprite(window, sprite, NULL);
	}
	sfTexture_destroy(texture);
	sfSprite_destroy(sprite);
}
