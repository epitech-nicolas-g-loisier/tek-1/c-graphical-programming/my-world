/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Opens the file given to start the program
*/

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "my_world.h"
#include "my.h"

int	check_line(char *line)
{
	int v_count = 0;
	int count = 0;

	while (line[count] != '\0' && line[count] != '\n') {
		while (line[count] == ' ' || line[count] == '\t')
			count++;
		if (line[count] == '-')
			count++;
		else if (line[count] < '0' || line[count] > '9')
			return (0);
		while (line[count] >= '0' && line[count] <= '9')
			count++;
		v_count++;
	}
	return (v_count);
}

sfVector2i	check_data(int fd)
{
	sfVector2i size = {0, 0};
	int v_count = 0;
	char *line = NULL;

	line = get_next_line(fd);
	while (line) {
		v_count = check_line(line);
		if (size.x == 0)
			size.x = v_count;
		if (size.x != v_count || v_count == 0) {
			write(2, "error: invalid data in savefile\n", 32);
			return ((sfVector2i){-1, -1});
		}
		free(line);
		line = get_next_line(fd);
		size.y++;
	}
	close(fd);
	return (size);
}

int	check_file(char *path)
{
	char *ext = ".legend";
	int fd;
	int count = my_strlen(path) - 1;
	int idx = 6;

	while (idx >= 0) {
		if (path[count] != ext[idx]) {
			write(2, "error: invalid type of file\n", 28);
			return (-1);
		}
		count = count - 1;
		idx = idx - 1;
	}
	fd = open(path, O_RDWR);
	if (fd == -1)
		write(2, "error: invalid path to file\n", 28);
	return (fd);
}

int	my_world(char *path)
{
	sfVector2i size;
	int **map = NULL;
	int fd;

	fd = check_file(path);
	if (fd == -1)
		return (84);
	size = check_data(fd);
	if (size.x <= 0)
		return (84);
	map = get_map(path, size);
	size.x--;
	size.y--;
	map = check_values(map, &size);
	save_map(NULL, path, NULL);
	return (open_window(map, &size));
}
