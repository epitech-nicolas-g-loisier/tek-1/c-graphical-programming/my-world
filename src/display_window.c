/*
** EPITECH PROJECT, 2018
** My World
** File description:
** open window
*/

#include <unistd.h>
#include <stdlib.h>
#include "my_world.h"

sfRenderWindow	*my_window_create(void)
{
	sfRenderWindow *window;
	sfVideoMode mode;

	mode.width = WIDTH;
	mode.height = HEIGHT;
	mode.bitsPerPixel = 32;
	window = sfRenderWindow_create(mode, "My World", sfFullscreen, NULL);
	sfRenderWindow_setFramerateLimit(window, 60);
	sfRenderWindow_setVerticalSyncEnabled(window, sfTrue);
	return (window);
}

int	destroy_data(int **map_3d, sfVector2f **map_2d, sfRenderWindow *win,
	sfVector2i *size)
{
	free_2d_map(map_2d, size);
	free_3d_map(map_3d, size);
	sfRenderWindow_destroy(win);
	play_music(1);
	play_sound(-1);
	return (0);
}

static int	print_err_mem_full(sfRenderWindow *window)
{
	sfRenderWindow_destroy(window);
	write(2, "error: memory full\n", 19);
	return (84);
}

void	draw_element(sfRenderWindow *window, int **map_3d,
			sfVector2f **map_2d, sfVector2i *size)
{
	sfColor color = {125, 125, 175, 255};

	sfRenderWindow_clear(window, color);
	draw_texture(window, map_2d, size);
	draw_map_border(window, map_2d, *size, map_3d);
	draw_button(window);
	draw_selected_button(window);
}

int	open_window(int **map_3d, sfVector2i *size)
{
	sfRenderWindow *window = my_window_create();
	sfVector2f **map_2d;
	sfVector2i move = {0, 0};

	if (map_3d == NULL)
		return (print_err_mem_full(window));
	map_2d = create_2d_map(map_3d, size);
	play_music(0);
	while (sfRenderWindow_isOpen(window)) {
		map_2d = update_2d_map(map_2d, size, map_3d);
		map_2d = move_map(map_2d, size, &move);
		draw_element(window, map_3d, map_2d, size);
		get_keyboard_event(&move, 0, map_2d, size);
		get_mouse_event(window, map_3d,& map_2d[0],
				(sfVector2i *[2]){size, &move});
		sfRenderWindow_display(window);
	}
	return (destroy_data(map_3d, map_2d, window, size));
}
