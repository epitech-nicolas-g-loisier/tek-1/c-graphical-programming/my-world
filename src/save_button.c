/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Save button
*/

#include "my_world.h"

int	save_button(sfRenderWindow *window, int **map3d, sfVector2i *size)
{
	sfVector2f pos = {1775, 150};
	sfVector2i mouse = sfMouse_getPosition((sfWindow*)window);

	if (sfMouse_isButtonPressed(sfMouseLeft) == sfTrue
		&& check_mouse_pos(mouse, pos) == 1) {
		save_map(map3d, NULL, size);
		return (1);
	}
	else if (sfKeyboard_isKeyPressed(sfKeyS) == sfTrue) {
		save_map(map3d, NULL, size);
		return (1);
	}
	return (0);
}
