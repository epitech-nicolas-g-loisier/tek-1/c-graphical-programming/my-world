/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Resets the move speed and the scaling
*/

#include "my_world.h"

void	reset_viewpoint(int **map_3d, sfVector2i *size)
{
	const int scales[3] = {SCALING_X, SCALING_Y, SCALING_Z};

	get_keyboard_event(NULL, 5, NULL, NULL);
	project_iso_point(0, 0, 0, (int [3]){scales[0], scales[1], scales[2]});
	draw_right_square(map_3d, NULL, (int [2]){0, scales[2]}, *size);
	draw_left_square(map_3d[0], NULL, 0, scales[2]);
}

void	set_viewpoint(int **map_3d, sfVector2i *size)
{
	get_keyboard_event(NULL, 10, NULL, NULL);
	get_zoom_input(map_3d, size, 1);
}
