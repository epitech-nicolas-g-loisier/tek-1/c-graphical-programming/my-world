/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Draw Button
*/

#include "my_world.h"

int	check_mouse_pos(sfVector2i mouse_pos, sfVector2f button_pos)
{
	if (mouse_pos.x > (button_pos.x + 100) ||
		mouse_pos.y > (button_pos.y + 100)) {
		return (0);
	}
	if (mouse_pos.x < button_pos.x || mouse_pos.y < button_pos.y) {
		return (0);
	}
	return (1);
}

int	pressed_button(sfRenderWindow *window, sfVector2i mouse)
{
	sfVector2f pos[9] = {{25, 25}, {25, 150}, {25, 275}, {25, 400},
				{25, 525}, {25, 650}, {1775, 25}, {1775, 150},
				{1775, 955}};
	char *str = "asset/button_state_click.png\0";
	int i = 0;

	while (i < 9) {
		if (check_mouse_pos(mouse, pos[i]) == 1) {
			draw_button_sprite(window, str, pos[i]);
			selected_button(i, 1);
			return (1);
		}
		i++;
	}
	return (0);
}

int	check_button_state(sfRenderWindow *window)
{
	sfVector2i mouse = sfMouse_getPosition((sfWindow*)window);
	int ret = 0;

	if (sfMouse_isButtonPressed(sfMouseLeft) == sfTrue) {
		ret = pressed_button(window, mouse);
	}
	else {
		ret = highlighted_button(window, mouse);
	}
	return (ret);
}

void	draw_button_sprite(sfRenderWindow *window, char *name, sfVector2f pos)
{
	sfTexture *texture;
	sfSprite *sprite = sfSprite_create();

	texture = sfTexture_createFromFile(name, NULL);
	sfSprite_setTexture(sprite, texture, sfTrue);
	sfSprite_setPosition(sprite, pos);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfTexture_destroy(texture);
	sfSprite_destroy(sprite);
}

void	draw_button(sfRenderWindow *window)
{
	char str[9][32] = {"asset/button_terra_up.png\0",
				"asset/button_terra_down.png\0",
				"asset/button_terra_set_zero.png\0",
				"asset/button_terra_unselect.png\0",
				"asset/button_select_corner.png\0",
				"asset/button_select_tile.png\0",
				"asset/button_exit.png\0",
				"asset/button_save.png\0",
				"asset/button_observer.png\0"};
	sfVector2f pos[9] = {{25, 25}, {25, 150}, {25, 275}, {25, 400},
				{25, 525}, {25, 650}, {1775, 25}, {1775, 150},
				{1775, 955}};
	int i = 0;

	while (i < 9) {
		draw_button_sprite(window, str[i], pos[i]);
		i++;
	}
}
