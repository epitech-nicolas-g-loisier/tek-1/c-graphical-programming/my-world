/*
** EPITECH PROJECT, 2018
** My World
** File description:
** create your own terraformer program
*/

#include <unistd.h>
#include "my_world.h"
#include "my.h"

void	print_usage(void)
{
	write(1, "Terraformer program\n\n", 21);
	write(1, "USAGE\n", 6);
	write(1, "\t./my_world filepath\n", 21);
	write(1, "\tfilepath: path to the save file.\n\n", 35);
	write(1, "OPTIONS\n", 8);
	write(1, "\t-h print the usage and quit.\n", 30);
}

int	check_error(int argc)
{
	if (argc != 2) {
		write(2, "error: ./my_world takes one parameter\n", 38);
		write(2, "retry with -h\n", 14);
		return (1);
	}
	return (0);
}

int	main(int argc, char **argv)
{
	if (argc == 2 && my_strcmp(argv[1], "-h") == 0) {
		print_usage();
		return (0);
	} else if (check_error(argc) == 1)
		return (84);
	return (my_world(argv[1]));
}
