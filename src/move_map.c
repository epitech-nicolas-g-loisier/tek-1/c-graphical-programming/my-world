/*
** EPITECH PROJECT, 2018
** My World
** File description:
** Function to get the move of the map
*/

#include <stdlib.h>
#include "my_world.h"

sfVector2f **move_map(sfVector2f **map, sfVector2i *size, sfVector2i *move)
{
	int idx_y = 0;
	int idx_x = 0;

	while (idx_y <= size->y) {
		idx_x = 0;
		while (idx_x <= size->x) {
			map[idx_y][idx_x].x += move->x;
			map[idx_y][idx_x].y += move->y;
			idx_x++;
		}
		idx_y++;
	}
	return (map);
}
