/*
** EPITECH PROJECT, 2017
** my_strncat
** File description:
** Concatenates n characters into a dest string
*/

char	*my_strncat(char *dest, char const *src, int nb)
{
	int argc = 0;
	int length = 0;

	while (dest[length] != '\0')
		length = length + 1;
	while (src[argc] != '\0' && argc < nb) {
		dest[length + argc] = src[argc];
		argc = argc + 1;
	}
	dest[length + argc] = '\0';
	return (dest);
}
