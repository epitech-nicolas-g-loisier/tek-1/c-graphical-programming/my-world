/*
** EPITECH PROJECT, 2017
** my_strcapitalize
** File description:
** Capitalizes the first letter of words
*/

int	find_if_in_word(int char_v)
{
	int in_word = 1;

	if (char_v < 48 || char_v > 122)
		in_word = 0;
	if (char_v < 65 && char_v > 57)
		in_word = 0;
	if (char_v < 97 && char_v > 90)
		in_word = 0;
	return (in_word);
}

char	*my_strcapitalize(char *str)
{
	int in_word = 0;
	int argc = 0;

	while (str[argc] != '\0') {
		if (str[argc] > 64 && str[argc] < 91) {
			str[argc] = str[argc] + 32;
		}
		if (in_word == 0 && str[argc] > 96 && str[argc] < 123)
			str[argc] = str[argc] - 32;
		in_word = find_if_in_word(str[argc]);
		argc = argc + 1;
	}
	str[argc] = '\0';
	return (str);
}
