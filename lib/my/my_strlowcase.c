/*
** EPITECH PROJECT, 2017
** my_strlowcase
** File description:
** Turns letter in lowercase
*/

char	*my_strlowcase(char *str)
{
	int argc = 0;

	while (str[argc] != '\0') {
		if (str[argc] > 64 && str[argc] < 91)
			str[argc] = str[argc] + 32;
		argc = argc + 1;
	}
	return (str);
}
