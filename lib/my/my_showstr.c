/*
** EPITECH PROJECT, 2017
** my_showstr
** File description:
** Prints a string even if non-printable and return 0
*/

#include <unistd.h>

void	print_lhex(char c, int *counter)
{
	int ifhider = 0;

	while (ifhider == 0) {
		if (c == 'v') {
			write(1, "0b", 2);
			*counter = *counter + 1;
		}
		if (c == 'f') {
			write(1, "0c", 2);
			*counter = *counter + 1;
		}
		ifhider = ifhider + 1;
	}
	if (c == 'r') {
		write(1, "0d", 2);
		*counter = *counter + 1;
	}
}

void	print_nhex(char c, int *counter)
{
	int ifhider = 0;
	char abprint = c - 42;

	while (ifhider == 0) {
		if (c == 'a' || c == 'b') {
			write(1, "0", 1);
			write(1, &abprint, 1);
			*counter = *counter + 1;
		}
		if (c == 't') {
			write(1, "09", 2);
			*counter = *counter + 1;
		}
		ifhider = ifhider + 1;
	}
	if (c == 'n') {
		write(1, "0a", 2);
		*counter = *counter + 1;
	}
	print_lhex(c, counter);
}

void	is_printable(int *counter, char c, char nextc)
{
	if (c < 127 && c > 31)
		write(1, &c, 1);
	if (c == '\\')
		print_nhex(nextc, counter);
}

int	my_showstr(char const *str)
{
	int counter = 0;

	while (str[counter] != '\0') {
		if (str[counter] == '\\' && str[counter + 1] == '0') {
			write(1, "\\0", 2);
			counter = counter + 1;
		}
		is_printable(&counter, str[counter], str[counter + 1]);
		counter = counter + 1;
	}
	return (0);
}
