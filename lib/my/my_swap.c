/*
** EPITECH PROJECT, 2017
** my_swap
** File description:
** Swap content of two int
*/

void	my_swap(int *a, int *b)
{
	int nb1 = *a;

	*a = *b;
	*b = nb1;
}
