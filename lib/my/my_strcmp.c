/*
** EPITECH PROJECT, 2017
** my_strcmp
** File description:
** Compares 2 strings
*/

int	my_strcmp(char const *s1, char const *s2)
{
	int argc = 0;

	while (s1[argc] - s2[argc] == 0) {
		if (s1[argc] == '\0' && s2[argc] == '\0')
			return (0);
		argc = argc + 1;
	}
	return (s1[argc] - s2[argc]);
}
