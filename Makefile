##
## EPITECH PROJECT, 2017
## My World
## File description:
## Makefile
##

SRC_DIR	=	$(realpath src)

CFLAGS	=	-Wall -Wextra -Iinclude  -L lib/ -lmy -lc_graph_prog -lm

SRC	=	$(SRC_DIR)/main.c	\
		$(SRC_DIR)/create_2d_map.c	\
		$(SRC_DIR)/display_window.c	\
		$(SRC_DIR)/draw_2d_map.c	\
		$(SRC_DIR)/project_iso_point.c	\
		$(SRC_DIR)/my_world.c	\
		$(SRC_DIR)/get_next_line.c	\
		$(SRC_DIR)/get_map.c	\
		$(SRC_DIR)/map_border.c	\
		$(SRC_DIR)/create_map_texture.c	\
		$(SRC_DIR)/move_map.c	\
		$(SRC_DIR)/check_event.c	\
		$(SRC_DIR)/update_2d_map.c	\
		$(SRC_DIR)/check_values.c	\
		$(SRC_DIR)/click_function.c	\
		$(SRC_DIR)/draw_button.c	\
		$(SRC_DIR)/draw_button_selected.c	\
		$(SRC_DIR)/draw_button_highlighted.c	\
		$(SRC_DIR)/find_tile_clicked.c	\
		$(SRC_DIR)/find_corner_clicked.c	\
		$(SRC_DIR)/save_map.c	\
		$(SRC_DIR)/save_button.c	\
		$(SRC_DIR)/close_button.c	\
		$(SRC_DIR)/close_popup.c	\
		$(SRC_DIR)/play_music.c	\
		$(SRC_DIR)/shortcuts_button.c	\
		$(SRC_DIR)/check_clock_init.c	\
		$(SRC_DIR)/get_zoom_input.c	\
		$(SRC_DIR)/is_obs_button_clicked.c	\
		$(SRC_DIR)/reset_viewpoint.c	\
		$(SRC_DIR)/get_map_point.c

OBJ	=	$(SRC:.c=.o)

NAME	=	my_world

all:            $(NAME)

$(NAME):	$(OBJ)
		make -C lib/my/
		gcc -o $(NAME) $(OBJ) $(CFLAGS)

tests_run:
		make -C tests

clean:
		rm -f $(OBJ)
		make clean -C lib/my/

fclean:		clean
		rm -f $(NAME)
		make fclean -C lib/my/

re:		fclean all
