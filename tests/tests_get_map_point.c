/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Tests the get_map_point function
*/

#include <criterion/criterion.h>
#include <SFML/Graphics.h>
#include <stdlib.h>
#include "my_world.h"

Test(get_map_point, handle_null)
{
	get_map_point(NULL, 0, 0, 0);
}
