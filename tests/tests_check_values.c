/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Tests the check_values function
*/

#include <criterion/criterion.h>
#include <SFML/Graphics.h>
#include "my_world.h"

Test(check_values, correct_values)
{
	sfVector2i size = {3, 2};
	int arr1[4] = {0, 1, 2, 3};
	int arr2[4] = {1, 2, 3, 4};
	int arr3[4] = {2, 3, 4, 5};
	int *arr[3] = {&arr1[0], &arr2[0], &arr3[0]};
	int **narr = NULL;

	narr = check_values(arr, &size);
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			if (arr[i][j] != narr[i][j])
				cr_assert(false, "Value modified when not needed");
		}
	}
}

Test(check_values, incorrect_values)
{
	sfVector2i size = {3, 2};
	int arr1[4] = {0, 10, 2, 300};
	int arr2[4] = {1, 2, -3, 4};
	int arr3[4] = {-21, 32, 4, 5};
	int *arr[3] = {&arr1[0], &arr2[0], &arr3[0]};
	int **narr = NULL;

	narr = check_values(arr, &size);
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			if (arr[i][j] <= MAX_Z && arr[i][j] >= MIN_Z) {
				if (arr[i][j] != narr[i][j])
					cr_assert(false, "Value modified when not needed");
			} else {
				if (narr[i][j] < MIN_Z || narr[i][j] > MAX_Z)
					cr_assert(false, "Value not in range");
			}		
		}
	}
}

Test(check_values, handle_null)
{
	check_values(NULL, NULL);
}
