/*
** EPITECH PROJECT, 2018
** my_world
** File description:
** Tests the check_init_clock function
*/

#include <criterion/criterion.h>
#include <SFML/Graphics.h>
#include "my_world.h"

Test(check_init_clock, is_clock_initiated)
{
	sfClock *clock = NULL;
	int time = 0;

	clock = check_init_clock(clock, &time);
	cr_assert(clock != NULL, "Clock not created");
	cr_assert(time == 3, "Time set incorrectly");
}

Test(check_init_clock, is_clock_time_read)
{
	sfClock *clock = sfClock_create();
	int time = 0;

	check_init_clock(clock, &time);
	cr_assert(time == 0, "Time not read");
}

Test(check_init_clock, handle_null)
{
	check_init_clock(NULL, NULL);
}
