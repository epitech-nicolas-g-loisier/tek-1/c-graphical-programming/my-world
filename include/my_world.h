/*
** EPITECH PROJECT, 2018
** My World
** File description:
** My World
*/

#include <SFML/Graphics.h>
#include <SFML/Audio.h>
#include <stdbool.h>

#ifndef MY_WORLD_H_
#define MY_WORLD_H_

#define WIDTH 1920
#define HEIGHT 1080

#define SCALING_X 64
#define SCALING_Y 64
#define SCALING_Z 8

#define MIN_Z -16
#define MAX_Z 32

bool	is_obs_button_clicked(sfRenderWindow *);

int	open_window(int **, sfVector2i *);

int	get_zoom_input(int **, sfVector2i *, int);

void	take_down_coord(int **, sfVector2i *);

void	get_keyboard_event(sfVector2i *, int, sfVector2f**, sfVector2i*);

void	get_mouse_event(sfRenderWindow *, int**, sfVector2f**, sfVector2i**);

void	draw_map_border(sfRenderWindow *, sfVector2f **, sfVector2i, int**);

void	draw_texture(sfRenderWindow *, sfVector2f **, sfVector2i *);

void	free_2d_map(sfVector2f **, sfVector2i *);

void	free_3d_map(int **, sfVector2i *);

void	reset_viewpoint(int **, sfVector2i *);

void	set_viewpoint(int **, sfVector2i *);

sfConvexShape	*draw_right_square(int **, sfVector2f **, int [2], sfVector2i);

sfConvexShape	*draw_left_square(int *, sfVector2f *, int, int);

sfVector2f	get_map_point(sfVector2f **, int, int, int);

sfVector2f	**move_map(sfVector2f **, sfVector2i *, sfVector2i *);

sfVector2f	**create_2d_map(int **, sfVector2i *);

sfVector2f	**update_2d_map(sfVector2f **, sfVector2i *, int **);

sfVector2f	**center_map(sfVector2f **, sfVector2i *);

sfVector2f	project_iso_point(int, int, int, int [3]);

char	*get_next_line(int);

int	**get_map(char *, sfVector2i);

int	**check_values(int **, sfVector2i *);

int	draw_2d_map(sfRenderWindow *, sfVector2f **, sfVector2i);

int	my_world(char *);

sfClock	*check_init_clock(sfClock *, int *);

sfVertexArray	*create_line(sfVector2f *, sfVector2f *);

sfVector2i	find_tile_clicked(sfVector2i, sfVector2f**,const sfVector2i*);

sfVector2i	find_corner_clicked(sfVector2i, sfVector2f**,sfVector2i*);

sfVector2i	selected_button(int, int);

void	draw_button(sfRenderWindow*);

void	draw_selected_button(sfRenderWindow*);

int	check_button_state(sfRenderWindow*);

int	**check_map_hit(sfRenderWindow*, int**, sfVector2f**, sfVector2i*);

void	draw_button_sprite(sfRenderWindow*, char*, sfVector2f);

int	highlighted_button(sfRenderWindow*, sfVector2i);

int	check_mouse_pos(sfVector2i, sfVector2f);

int     save_map(int * const *, const char*, sfVector2i*);

int	save_button(sfRenderWindow*, int**, sfVector2i*);

int	close_button(sfRenderWindow*, int**, sfVector2f**, sfVector2i*);

void	draw_element(sfRenderWindow*, int**, sfVector2f**, sfVector2i*);

int	display_save_popup(sfRenderWindow*, int**, sfVector2f**, sfVector2i*);

void	play_music(int);

void	play_sound(int);

void	get_move(sfVector2i*, int**);

void	get_shortcuts(void);

bool    tile_is_inbound(sfVector2f**, sfVector2i*);

#endif /* MY_WORLD_H_ */
